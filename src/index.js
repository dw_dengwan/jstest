"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
{
    let num = 86;
    let num1 = new Number(86);
    let num2 = 11;
    // let num3:number =  new Number(11); //error
    let str = "43";
    let und = undefined;
    let bol = false;
    let unk = null;
    bol = unk;
    let any = null;
    und = any;
    let nul = null;
    let sym = Symbol("sym");
    let obj = { name: "张三", age: 18 };
    let obj2 = { name: "张三", age: 18 };
    let arr = [1, 2, 3];
    let arr2 = [1, 2, 3];
    let tuple = [1, "2"];
    let Sex;
    (function (Sex) {
        Sex[Sex["man"] = 0] = "man";
        Sex[Sex["woman"] = 1] = "woman";
    })(Sex || (Sex = {}));
    ;
    let fn = (a, b) => a + b;
    let fnError = () => { throw new Error("error"); };
    let fnVoid = () => { console.log("void"); };
    let str1 = '1';
    // type numType = typeof 1;//error
    let obj1 = { name: "张三", age: 18, sex: Sex.man };
    let obj3 = { name: "张三", age: 18 };
    ;
    let obj4 = { name: "张三", age: 18 };
    let obj5 = { name: "张三", age: 18 };
    let test = 111;
    // test = 222;//error
    if (typeof obj5 === "object" && obj5 !== null) {
        // obj5.name; //error
    }
    // (obj5 as Object).name; // 报错
    let fnNumAndStr = (a) => {
        a.toFixed();
        if (typeof a === "string") {
            a.length;
        }
    };
    let fnPromise = () => { return new Promise((resolve, reject) => { resolve(1); }); };
    let fnAsync = () => __awaiter(void 0, void 0, void 0, function* () { return 1; });
    let fnAsync2 = () => __awaiter(void 0, void 0, void 0, function* () { return yield Promise.resolve(1); });
}
