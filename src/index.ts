{
    let num:number = 86;
    let num1:Number = new Number(86);
    let num2:Number = 11;
    // let num3:number =  new Number(11); //error
    let str:string = "43";
    let und:undefined = undefined;
    let bol:boolean = false;
    let unk:unknown = null;
    bol = unk as boolean;
    let any:any = null;
    und = any;
    let nul:null = null;
    let sym:symbol = Symbol("sym");
    let obj:{name:string, age?:number} = {name:"张三", age:18};
    let obj2:{name:string, [propName:string]:any} = {name:"张三", age:18};
    let arr:number[] = [1,2,3];
    let arr2:Array<number> = [1,2,3];
    let tuple:[number, string] = [1, "2"];
    enum Sex {man, woman};
    let fn:(a:number, b:number) => number = (a, b) => a + b;
    let fnError:() => never = () => {throw new Error("error")};
    let fnVoid:() => void = () => {console.log("void")};
    type Person = {name:string, age:number};
    type Info = Person & {sex:Sex};
    type strType = '1'
    let str1:strType = '1';
    // type numType = typeof 1;//error
    let obj1:Info = {name:"张三", age:18, sex:Sex.man};
    let obj3:Person = {name:"张三", age:18};
    interface Person2 {name:string, age:number};
    let obj4:Person2 = {name:"张三", age:18};
    let obj5:unknown = {name:"张三", age:18};
    let test:111 = 111;
    // test = 222;//error
    if(typeof obj5 === "object" && obj5 !== null){
        // obj5.name; //error
    }
    // (obj5 as Object).name; // 报错
    let fnNumAndStr: (a:number|string) => void = (a) => {
        (a as number).toFixed();
        if(typeof a === "string"){
            a.length;
        }
    };

    let fnPromise:() => Promise<number> = () => {return new Promise((resolve, reject) => {resolve(1)})};
    let fnAsync:() => Promise<number> = async () => {return 1};
    let fnAsync2:() => Promise<number> = async () => {return await Promise.resolve(1)};
}